<?php
function myra_add_email_box()
{
    $screens = ['page'];
    foreach ($screens as $screen) {
        add_meta_box(
            'myra_form_email',           // Unique ID
            'Myra Email for form',  // Box title
            'myra_custom_box_html',  // Content callback, must be of type callable
            $screens,                   // Post type
            'side'
        );
    }
}
add_action('add_meta_boxes', 'myra_add_email_box');

function myra_custom_box_html($post)
{
    $email = get_post_meta($post->ID, 'myra_form_email', true);
    ?>
    <label for="wporg_field">Add an email account</label>
    <input type="email" name="myra_form_email" value="<?php echo $email ?>">
    <?php
}

function myra_save_postdata($post_id)
{
    if (array_key_exists('myra_form_email', $_POST)) {
        update_post_meta(
            $post_id,
            'myra_form_email',
            $_POST['myra_form_email']
        );
    }
}
add_action('save_post', 'myra_save_postdata');

function myra_theme_enqueue_styles() {

    global $post;

    $email = get_post_meta($post->ID, 'myra_form_email', true);

    // Localize the script with new data
    $myra_array = array(
        'page_url' => home_url(),
        'mailto_email' => $email
    );

    wp_enqueue_style( 'divi-parent-style', get_template_directory_uri() . '/style.css' );
    // This was added hard coded to avoid the Divi minified styles importance
    wp_enqueue_style( 'divi-common-sections-style', get_stylesheet_directory_uri() . '/css/myra-common-sections-style.css', '', NULL, NULL );
    wp_enqueue_script(
        'myra-sticky-tool', // Handler
        get_stylesheet_directory_uri() . '/js/jquery.sticky.js', // Path
        array('jquery'), // Depends on jQuery
        NULL, // Removed version argument
        TRUE // Loads in footer
    );
    wp_enqueue_script(
        'myra-owlcarousel', // Handler
        get_stylesheet_directory_uri() . '/owlcarousel/owl.carousel.min.js', // Path
        array('jquery'), // Depends on jQuery
        NULL, // Removed version argument
        TRUE // Loads in footer
    );
    wp_register_script(
        'myra-custom-scripts', // Handler
        get_stylesheet_directory_uri() . '/js/myra-custom-scripts.js', // Path
        array('jquery'), // Depends on jQuery
        NULL, // Removed version argument
        TRUE // Loads in footer
    );

    wp_localize_script( 'myra-custom-scripts', 'myra_stuff', $myra_array );

    // Enqueued script with localized data.
    wp_enqueue_script( 'myra-custom-scripts' );
}
add_action( 'wp_enqueue_scripts', 'myra_theme_enqueue_styles' );
