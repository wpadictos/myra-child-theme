/**
// Fixing navigation on top
//
**/
jQuery(document).ready( function() {
    console.log('Myra App is running');
    var textareaCounterHtml = '';
    var textareaMinChars = 0;
    var textareaMaxChars = 500;
    var textareaInput = jQuery('textarea[data-original_id="message"]');
    var ta_remaining = null;

    /* HOME SLIDER */
    jQuery('#myra-home-slider').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        items: 1,
        autoplay:true,
        autoplayTimeout:1000,
        autoplayHoverPause:true
    })



    if(jQuery(window).width() >= 960){
        jQuery(".myra-product-nav-sticky").sticky({
            topSpacing: 86,
            className: 'myra-is-sticky',
            wrapperClassName: 'myra-sticky-wrapper'
        });
    }

    jQuery(window).scroll( function() {
        if ( jQuery('#main-header').is('.et-fixed-header') ){
            jQuery('body').addClass('hide-elements-on-navbar');
        }else{
            jQuery('body').removeClass('hide-elements-on-navbar');
        }
    });

    textareaCounterHtml += '<div id="textarea-counter" class="textarea-counter text-myra-blue">';
    textareaCounterHtml += '<p><span id="ta_remaining">' + textareaMinChars + '</span> / <span id="ta_maximun">' + textareaMaxChars + '</span> Zeichenzahl</p>';
    textareaCounterHtml += '</div>';

    jQuery('p[data-id="message"]').append( textareaCounterHtml );

    ta_remaining = jQuery('#ta_remaining');

    /* if textarea of product contact form changes */
    textareaInput.on('keyup', function(e){
        textareaMinChars = this.value.length
        var text = this.value;
        ta_remaining.html( textareaMinChars );
        if ( textareaMinChars >= 500 ){
            jQuery('#textarea-counter').addClass('text-exceed-limit');
            this.value = text.substr(0,500);
        }else{
            jQuery('#textarea-counter').removeClass('text-exceed-limit');
        }
    })

    /* Remove button from product contact form and add a new one */
    var productContactButtonText = 'Abschicken';
    var productContactButton = '<button type="submit" class="et_pb_contact_submit et_pb_button myra-product-submit"><span class="myra-product-submit-text">' + productContactButtonText + '</span><span class="myra-product-submit-bullet"></span></button>';
    jQuery('.myra-product-contact-form .et_pb_contact_submit').remove();
    jQuery('.myra-product-contact-form .et_contact_bottom_container').append( productContactButton );

    /* NOTFALL box activation */
    jQuery('#myra-notfall-btn').click( function (){
        if ( jQuery('#notfall-textwidget').is(':hidden') ){
            jQuery('#notfall-textwidget').slideDown('fast');
        }else{
            jQuery('#notfall-textwidget').hide();
        };
    })

    /* Product Overview page PRODUCT ASSETS */
    var productAssetsCards = jQuery('.product-assets-card');
    jQuery.each( productAssetsCards, function(index, value){
        var html = '';
        html += '<div class="product-asset-card-button-placeholder">';
        html += '<span class="product-asset-card-button"></span>';
        html += '</div>';
        jQuery(this).append( html );
    })

    /* MYRA SEND MAILS */
		jQuery(document).on('submit', function(e){
			e.preventDefault();

			var data = [];

			// get inputs values */
			var inputs = jQuery('.et_pb_contact_form').find(':input');
			var process = true;

			jQuery.each( inputs, function( index, value ){
				if ( !jQuery(this).hasClass('et_contact_error') && process ){
					if ( jQuery(this).hasClass('input') ) {
						var dataObj = {
							elementName : jQuery(this).attr('name'),
							value : jQuery(this).val()
						}
						data.push( dataObj );
					}
				}else{
					process = false;
				}
			});
			if ( process ) {
				jQuery.ajax({
					url: myra_stuff.page_url + '/wp-content-themes/myra-chikd-theme/includes/myra_send_mail.php',
					method: 'post',
					type: 'json',
					data: {
						data 	: data,
					},
					beforeSend: function(){
					},
					success: function( response ){
						console.log( response );
					},
					error: function( response ){
						console.error( response );
					}

				});
			}

		});

})
