<?php

define('__ROOT__', dirname(dirname(__FILE__)));

/* Namespace alias. */
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require __ROOT__ . '/myra-send-mail/php-mailer/Exception.php';
require __ROOT__ . '/myra-send-mail/php-mailer/PHPMailer.php';
require __ROOT__ . '/myra-send-mail/php-mailer/SMTP.php';

if( $_POST ){

    $totalmessage = '';
    $anrede     = $_POST['data'][0]['value'];
    $name       = $_POST['data'][1]['value'];
    $email      = $_POST['data'][2]['value'];
    $telefon    = $_POST['data'][3]['value'];
    $message    = $_POST['data'][4]['value'];

	$mail = new PHPMailer(TRUE); // Create a new PHPMailer object. Passing TRUE to the constructor enables exceptions.

	try {
	   $mail->setFrom($email, $name); // Set the mail sender.
	   $mail->addAddress('info@msec.locallhost.de', 'Myra Customer Service'); // Add a recipient.
	   $mail->Subject = 'MYRA - Service Request'; // Set the subject.
	   $mail->Body = $message; // Set the mail message body.
	   $mail->send(); // Finally send the mail.
	}
	catch (Exception $e)
	{
	   echo $e->errorMessage(); // PHPMailer exception.
	}
	catch (\Exception $e)
	{
	   echo $e->getMessage(); // PHP exception (note the backslash to select the global namespace Exception class).
	}

}else{
	echo 'false';
}
